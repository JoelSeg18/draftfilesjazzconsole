﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Configuration;
using EASendMail;

namespace DraftFilesJazz
{
    class Program
    {
        static void Main(string[] args)
        {

            string rootPath = ConfigurationManager.AppSettings["path"]; 

            try
            {
                List<string> routes = new List<string>();
                var builder = new StringBuilder();

                for (int i = 0; i < rootPath.Length; i++)
                {

                    if (rootPath.Substring(i, 1) == "-")
                    {
                        Console.WriteLine("{0}", builder); //optional
                        routes.Add(builder.ToString());
                        builder = new StringBuilder();
                    }
                    else
                    {
                        builder.Append(rootPath.Substring(i, 1));
                    }
                }

                routes.Add(builder.ToString());
                Console.WriteLine("{0}", builder); //optional

                foreach (string item in routes)
                {

                    string[] files = Directory.GetFiles(item);
                    foreach (string file in files)
                {
                    Console.WriteLine(file); //optional
                    FileInfo fi = new FileInfo(file);
                    if (fi.LastWriteTime < DateTime.Now.Date)
                        fi.Delete();
                }

                }
                Console.ReadLine();
            }
            catch (Exception ex)
            {

                try
                {
                    SmtpMail oMail = new SmtpMail("TryIt");

                    // Set sender email address
                    oMail.From = ConfigurationManager.AppSettings["addressFrom"];
                    // Set recipient email address, please change it to yours
                    oMail.To = ConfigurationManager.AppSettings["addressTo"];

                    // Set email subject
                    oMail.Subject = "Error deleting files";
                    // Set email body
                    oMail.TextBody = ex.Message;


                    // SMTP server address
                    SmtpServer oServer = new SmtpServer(ConfigurationManager.AppSettings["host"]);

                    // User and password for ESMTP authentication
                    oServer.User = ConfigurationManager.AppSettings["addressFrom"];
                    oServer.Password = ConfigurationManager.AppSettings["pass"];

                    // Most mordern SMTP servers require SSL/TLS connection now.
                    // ConnectTryTLS means if server supports SSL/TLS, SSL/TLS will be used automatically.
                    oServer.ConnectType = SmtpConnectType.ConnectTryTLS;

                    // Port
                    oServer.Port = Int16.Parse(ConfigurationManager.AppSettings["port"]);

                    SmtpClient oSmtp = new SmtpClient();
                    oSmtp.SendMail(oServer, oMail);

                    Console.WriteLine("Error deleting files, email was sent successfully!");
                }
                catch (Exception ep)
                {
                    Console.WriteLine("failed to send email with the following error:");
                    Console.WriteLine(ep.Message);
                }
            }

 

}
    }
}
